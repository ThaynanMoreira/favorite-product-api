const express = require('express');
const cookieParser = require('cookie-parser');
const addRequestId = require('express-request-id')();
const morgan = require('morgan');
const logger = require('./middlewares/logger');

const indexRouter = require('./routes/index');

const app = express();
const loggerFormat = ':id [:date[web]] ":method :url" :status :response-time';

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(addRequestId);

app.get('/health', (req, res) => {
  res.status(200).send();
});

morgan.token('id', (req) => req.id);


app.use(morgan(loggerFormat, {
  skip(req, res) {
    return res.statusCode < 400;
  },
  stream: process.stderr,
}));

app.use(morgan(loggerFormat, {
  skip(req, res) {
    return res.statusCode >= 400;
  },
  stream: process.stdout,
}));

app.use(logger.logRequisitionMiddleware);

app.use(logger.logResponseMiddleware);

app.use('/', indexRouter);

module.exports = app;
