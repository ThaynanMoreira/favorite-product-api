const express = require('express');
const HttpStatusCode = require('http-status-codes');

const router = express.Router();

router.get('/health', (req, res) => {
  let messageHealth = 'server running';
  if (process.env.NODE_ENV === 'production') {
    messageHealth = `server running with ${process.env.NODE_ENV} config`;
  }
  res.status(HttpStatusCode.OK).send(messageHealth);
});

router.use('/api', require('./api'));

module.exports = router;
