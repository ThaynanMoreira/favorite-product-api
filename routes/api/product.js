const express = require('express');
const Status = require('http-status-codes');
const Joi = require('joi');
const Product = require('../../controllers/product');
const { validate } = require('../../middlewares/validate');
const logger = require('../../middlewares/logger');

const router = express.Router();

const schema = Joi.object().keys({
  title: Joi.string().min(3).max(30)
    .required(),
  image: Joi.string().uri().trim().required(),
  price: Joi.number().required(),
  brand: Joi.string().min(3).max(30).required(),
});

const favoriteSchema = Joi.object().keys({
  clientId: Joi.number().required(),
});

const formatBody = (body) => ({
  title: body.title, price: body.price, image: body.image, brand: body.brand,
});

const defaultResponse = (data, statusCode = Status.OK, reqId = 1111) => {
  logger.logResponse(reqId, data, statusCode);
  return { data, statusCode };
};

const errorResponse = (message,
  statusCode = Status.INTERNAL_SERVER_ERROR, reqId = 2222) => {
  logger.logError(reqId, message, statusCode);
  return { data: { error: message }, statusCode };
};

/* GET users listing. */
router.get('/', (req, res) => Product.getAll(req.query.search)
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

// Create an user;
router.post('/', validate(schema), (req, res) => Product.create(formatBody(req.body))
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.put('/:productId/favorite', validate(favoriteSchema), (req, res) => Product.favoriteProduct(req.body.clientId, req.params.productId)
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json([1]);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.get('/:id', (req, res) => Product.getById(req.params.id)
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.put('/:id', validate(schema), (req, res) => Product.update(req.params.id, formatBody(req.body))
  .then(() => Product.getById(req.params.id))
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json([1]);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.delete('/:id', (req, res) => Product.delete(req.params.id)
  .then((response) => {
    const r = defaultResponse(response, Status.NO_CONTENT, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

module.exports = router;
