const express = require('express');
const Status = require('http-status-codes');
const Joi = require('joi');
const Review = require('../../controllers/review');
const { validate } = require('../../middlewares/validate');
const logger = require('../../middlewares/logger');

const router = express.Router();

const schema = Joi.object().keys({
  score: Joi.number().required(),
  description: Joi.string().max(255),
  ProductId: Joi.number().required(),
  ClientId: Joi.number().required(),
});

const formatBody = (body) => ({
  score: body.score,
  description: body.description,
  ProductId: body.ProductId,
  ClientId: body.ClientId,
});

const defaultResponse = (data, statusCode = Status.OK, reqId = 1111) => {
  logger.logResponse(reqId, data, statusCode);
  return { data, statusCode };
};

const errorResponse = (message,
  statusCode = Status.INTERNAL_SERVER_ERROR, reqId = 2222) => {
  logger.logError(reqId, message, statusCode);
  return { data: { error: message }, statusCode };
};

// Create an user;
router.post('/', validate(schema), (req, res) => Review.create(formatBody(req.body))
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.delete('/:id', (req, res) => Review.delete(req.params.id)
  .then((response) => {
    const r = defaultResponse(response, Status.NO_CONTENT);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

module.exports = router;
