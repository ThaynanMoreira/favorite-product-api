const express = require('express');
const Status = require('http-status-codes');
const Joi = require('joi');
const Client = require('../../controllers/client');
const { validate } = require('../../middlewares/validate');
const logger = require('../../middlewares/logger');

const router = express.Router();

const schema = Joi.object().keys({
  name: Joi.string().min(3).max(30)
    .required(),
  email: Joi.string().email({ minDomainSegments: 2 }).required(),
});

const defaultResponse = (data, statusCode = Status.OK, reqId = 1111) => {
  logger.logResponse(reqId, data, statusCode);
  return { data, statusCode };
};

const errorResponse = (message,
  statusCode = Status.INTERNAL_SERVER_ERROR, reqId = 2222) => {
  logger.logError(reqId, message, statusCode);
  return { data: { error: message }, statusCode };
};

/* GET users listing. */
router.get('/', (req, res) => Client.getAll(req.query.search)
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

// Create an user;
router.post('/', validate(schema), (req, res) => Client.create({ name: req.body.name, email: req.body.email })
  .then((response) => {
    const r = defaultResponse(response, Status.CREATED, req.id);

    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.get('/:id', (req, res) => Client.getById(req.params.id)
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.put('/:id', validate(schema), (req, res) => Client.update(req.params.id, req.body)
  .then(() => Client.getById(req.params.id))
  .then((response) => {
    const r = defaultResponse(response, Status.OK, req.id);
    res.status(r.statusCode);
    res.json([1]);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

router.delete('/:id', (req, res) => Client.delete(req.params.id)
  .then((response) => {
    const r = defaultResponse(response, Status.NO_CONTENT, req.id);
    res.status(r.statusCode);
    res.json(r.data);
  })
  .catch((error) => {
    const e = errorResponse(error.message, Status.INTERNAL_SERVER_ERROR, req.id);
    res.status(e.statusCode);
    res.json(e.data);
  }));

module.exports = router;
