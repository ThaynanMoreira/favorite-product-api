const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  const response = 'Hello to API';
  res.send(response);
});

router.use('/client', require('./client'));
router.use('/product', require('./product'));
router.use('/review', require('./review'));

module.exports = router;
