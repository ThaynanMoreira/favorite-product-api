module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    score: {
      allowNull: false,
      type: DataTypes.INTEGER(11),
      max: 10,
      min: 0,
    },
    description: DataTypes.STRING(255),
  }, { indexes: [{ unique: true, fields: ['ProductId', 'ClientId'] }] });
  Review.associate = (models) => {
    Review.belongsTo(models.Product, { foreignKey: 'ProductId' });
    Review.belongsTo(models.Client, { foreignKey: 'ClientId' });
  };
  return Review;
};
