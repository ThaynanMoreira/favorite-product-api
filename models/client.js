module.exports = (sequelize, DataTypes) => {
  const Client = sequelize.define('Client', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      isEmail: true,
      unique: true,
    },
  });
  Client.associate = (models) => {
    Client.belongsToMany(models.Product, {
      as: 'FavoriteProducts', through: 'ClientFavoriteProducts', foreignKey: 'ClientId', otherKey: 'ProductId',
    });
    Client.hasMany(models.Review, { as: 'ReviewsSent', foreignKey: 'ClientId' });
  };
  return Client;
};
