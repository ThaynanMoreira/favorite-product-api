const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config/config')[process.env.NODE_ENV];

const basename = path.basename(__filename);
const db = {};
// const config = {
//   database: 'database_development',
//   username: 'root',
//   password: 'password',
//   host: '127.0.0.1',
//   url: 'mysql://root:password@127.0.0.1/database_development',
//   dialect: 'mysql',
//   dialectOptions: { decimalNumbers: true },
// };
const sequelize = new Sequelize(config.database, config.username, config.password, config);

fs
  .readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
