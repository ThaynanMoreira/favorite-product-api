module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    image: DataTypes.STRING,
    brand: DataTypes.STRING,
    title: DataTypes.STRING,
    price: DataTypes.DECIMAL(13, 2),
  }, {});
  Product.associate = (models) => {
    Product.belongsToMany(models.Client, {
      as: 'ClientsFavored', through: 'ClientFavoriteProducts', foreignKey: 'ProductId', otherKey: 'ClientId',
    });
    Product.hasMany(models.Review, { as: 'ReviewsReceived', foreignKey: 'ProductId' });
  };
  return Product;
};
