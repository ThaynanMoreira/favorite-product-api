const Joi = require('joi');

module.exports.validate = (schema, property = 'body') => (req, res, next) => {
  const { error } = Joi.validate(req[property], schema);
  const valid = error == null;
  if (valid) {
    next();
  } else {
    const { details } = error;
    const message = details.map((i) => i.message).join(',');
    //   console.log("error", message);  LOG
    res.status(422).json({ error: message });
  }
};
