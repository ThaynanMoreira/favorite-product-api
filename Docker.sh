#!/bin/bash

# build the docker image
docker build -t favorite-product/api .

# run the docker image
docker run -p 8080:3000 -d favorite-product/api

# wait for app to start
sleep 5

# test
curl -i http://localhost:8080

# open in browser
open http://localhost:8080