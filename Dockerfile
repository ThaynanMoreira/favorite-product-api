FROM mhart/alpine-node:latest

RUN apk add g++ make python

WORKDIR /api

COPY . .

RUN yarn install
# RUN npm run migrate

EXPOSE 3000

# CMD ["npm", "start"]