module.exports = {
  up: async (queryInterface) => {
    const clients = await queryInterface.sequelize.query(
      'SELECT id from Clients;',
    );
    const products = await queryInterface.sequelize.query(
      'SELECT id from Products;',
    );

    const clientsRows = clients[0][0].id;
    const productsRows = products[0][0].id;
    return queryInterface.bulkInsert('ClientFavoriteProducts', [{
      ClientId: clientsRows,
      ProductId: productsRows,
    }], {});
  },

  down: (queryInterface) => queryInterface.bulkDelete('ClientFavoriteProducts', null, {}),
};
