
module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('Clients', [
    { name: 'John Doe', email: 'email.example@email.com' },
    { name: 'Doe Doe John', email: 'email.example2@email.com' },
  ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Clients', null, {})
  ,
};
