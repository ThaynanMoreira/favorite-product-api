module.exports = {
  up: async (queryInterface) => {
    const clients = await queryInterface.sequelize.query(
      'SELECT id from Clients;',
    );
    const products = await queryInterface.sequelize.query(
      'SELECT id from Products;',
    );
    const clientsRows = clients[0];
    const productsRows = products[0];
    return queryInterface.bulkInsert('Reviews', [{
      score: 9,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere augue et lobortis ullamcorper. Ut lacinia maximus tincidunt. In hac habitasse platea dictumst. Sed eget neque ligula. Maecenas scelerisque nunc et erat viverra efficitur. In hac metus.',
      ClientId: clientsRows[0].id,
      ProductId: productsRows[0].id,
    }, {
      score: 3,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere augue et lobortis ullamcorper. Ut lacinia maximus tincidunt. In hac habitasse platea dictumst. Sed eget neque ligula. Maecenas scelerisque nunc et erat viverra efficitur. In hac metus.',
      ClientId: clientsRows[1].id,
      ProductId: productsRows[0].id,
    }, {
      score: 1,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere augue et lobortis ullamcorper. Ut lacinia maximus tincidunt. In hac habitasse platea dictumst. Sed eget neque ligula. Maecenas scelerisque nunc et erat viverra efficitur. In hac metus.',
      ClientId: clientsRows[1].id,
      ProductId: productsRows[1].id,
    }, {
      score: 15,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere augue et lobortis ullamcorper. Ut lacinia maximus tincidunt. In hac habitasse platea dictumst. Sed eget neque ligula. Maecenas scelerisque nunc et erat viverra efficitur. In hac metus.',
      ClientId: clientsRows[0].id,
      ProductId: productsRows[1].id,
    }], {});
  },

  down: (queryInterface) => queryInterface.bulkDelete('Reviews', null, {}),
};
