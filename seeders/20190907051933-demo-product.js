module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('Products', [{
    title: 'Celular',
    image: 'http://i.mlcdn.com.br/portaldalu/fotosconteudo/50987.jpg',
    brand: 'Xiaomi',
    price: 999.90,
  },
  {
    title: 'Notebook',
    image: 'http://img.magazineluiza.com.br/produto_grande/20/208222800.jpg',
    brand: 'Assus',
    price: 1299.90,
  },
  {
    title: 'Fogão',
    image: 'https://c.mlcdn.com.br//fogao-4-bocas-consul-cfo4nar-inox-acendimento-automatico/v/210x210/214469000.jpg',
    brand: 'Consul',
    price: 700,
  }], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Products', null, {}),
};
