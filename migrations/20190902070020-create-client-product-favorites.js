
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('ClientFavoriteProducts', {
    ClientId: {
      primaryKey: true,
      allowNull: false,
      type: Sequelize.INTEGER(11),
      references: {
        model: 'Clients',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    ProductId: {
      primaryKey: true,
      allowNull: false,
      type: Sequelize.INTEGER(11),
      references: {
        model: 'Products',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('ClientFavoriteProducts'),
};
