
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Products', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER(11),
    },
    image: {
      allowNull: false,
      type: Sequelize.STRING(255),
      isUrl: true,
    },
    brand: {
      allowNull: false,
      type: Sequelize.STRING(45),
    },
    title: {
      allowNull: false,
      type: Sequelize.STRING(45),
    },
    price: {
      allowNull: false,
      type: Sequelize.DECIMAL(13, 2),
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('Products'),
};
