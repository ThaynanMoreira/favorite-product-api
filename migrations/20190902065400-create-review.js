
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Reviews', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER(11),
    },
    score: {
      allowNull: false,
      type: Sequelize.INTEGER(11).UNSIGNED,
      max: 10,
      min: 0,
    },
    description: {
      type: Sequelize.STRING(255),
    },
    ClientId: {
      type: Sequelize.INTEGER(11),
      references: {
        model: 'Clients',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    },
    ProductId: {
      allowNull: false,
      type: Sequelize.INTEGER(11),
      references: {
        model: 'Products',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()'),
    },
  }, {
    uniqueKeys: {
      ReviewUnique: {
        fields: ['ClientId', 'ProductId'],
      },
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('Reviews'),
};
