/* eslint-disable no-shadow */
const supertest = require('supertest');
const chai = require('chai');
const mocha = require('mocha');
const app = require('../../../../app');

const { it, describe } = mocha;
const request = supertest(app);
const { expect } = chai;

const defaultObj = {
  name: 'Default Client',
  email: 'defaultclient@email.com',
};
const otherObj = {
  name: 'Other Client',
  email: 'otherclient@email.com',
};
module.exports.test = () => {
  describe('Routes Clients', () => {
    new Promise((resolve) => {
      describe('Route POST /api/client', () => {
        it('Should create a client', (done) => {
          request
            .post('/api/client')
            .send(defaultObj)
            .end((err, res) => {
              Object.keys(defaultObj).forEach((item) => {
                expect(res.body[item]).to.be.eql(defaultObj[item]);
              });

              done(err);
              resolve(res.body.id);
            });
        });
      });
    }).then((id) => new Promise((resolve) => {
      describe('Route GET /api/client', () => {
        it('Should return a client list', (done) => {
          request
            .get('/api/client')
            .end((err, res) => {
              res.body.rows.forEach((item) => {
                if (item.id === id) {
                  Object.keys(defaultObj).forEach((property) => {
                    expect(item[property]).to.be.eql(defaultObj[property]);
                  });
                }
              });

              resolve(id);
              done(err);
            });
        });
      });
    }))
      .then((id) => new Promise((resolve) => {
        describe('Route GET /api/client/{id}', () => {
          it('Should return a client', (done) => {
            request
              .get(`/api/client/${id}`)
              .end((err, res) => {
                Object.keys(defaultObj).forEach((item) => {
                  expect(res.body[item]).to.be.eql(defaultObj[item]);
                });

                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route PUT /api/client/{id}', () => {
          it('Should update a client', (done) => {
            request
              .put(`/api/client/${id}`)
              .send(otherObj)
              .end((err, res) => {
                expect(res.body).to.be.eql([1]);
                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route GET /api/client/{id}', () => {
          it('Should return a client', (done) => {
            request
              .get(`/api/client/${id}`)
              .end((err, res) => {
                Object.keys(otherObj).forEach((item) => {
                  expect(res.body[item]).to.be.eql(otherObj[item]);
                });

                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route DELETE /api/client/{id}', () => {
          it('Should delete a client', (done) => {
            request
              .delete(`/api/client/${id}`)
              .end((err, res) => {
                expect(res.statusCode).to.be.eql(204);
                resolve();
                done(err);
              });
          });
        });
      }));
  });
};
