/* eslint-disable no-shadow */
const supertest = require('supertest');
const chai = require('chai');
const mocha = require('mocha');
const app = require('../../../../app');

const { it, describe } = mocha;
const request = supertest(app);
const { expect } = chai;

const defaultObj = {
  price: 16.11,
  image: 'http://www.defaultimgtest.com',
  brand: 'Default Blend',
  title: 'Default Product',
};
const otherObj = {
  price: 21.99,
  image: 'https://www.otherimgtest.com',
  brand: 'Other Blend',
  title: 'Other Product',
};

module.exports.test = () => {
  describe('Routes Products', () => {
    new Promise((resolve) => {
      describe('Route POST /api/product', () => {
        it('Should create a product', (done) => {
          request
            .post('/api/product')
            .send(defaultObj)
            .end((err, res) => {
              Object.keys(defaultObj).forEach((item) => {
                expect(res.body[item]).to.be.eql(defaultObj[item]);
              });

              done(err);
              resolve(res.body.id);
            });
        });
      });
    }).then((id) => new Promise((resolve) => {
      describe('Route GET /api/product', () => {
        it('Should return a product list', (done) => {
          request
            .get('/api/product')
            .end((err, res) => {
              res.body.rows.forEach((item) => {
                if (item.id === id) {
                  Object.keys(defaultObj).forEach((property) => {
                    expect(item[property]).to.be.eql(defaultObj[property]);
                  });
                }
              });

              resolve(id);
              done(err);
            });
        });
      });
    }))
      .then((id) => new Promise((resolve) => {
        describe('Route GET /api/product/{id}', () => {
          it('Should return a product', (done) => {
            request
              .get(`/api/product/${id}`)
              .end((err, res) => {
                Object.keys(defaultObj).forEach((item) => {
                  expect(res.body[item]).to.be.eql(defaultObj[item]);
                });

                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route PUT /api/product/{id}', () => {
          it('Should update a product', (done) => {
            request
              .put(`/api/product/${id}`)
              .send(otherObj)
              .end((err, res) => {
                expect(res.body).to.be.eql([1]);
                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route GET /api/product/{id}', () => {
          it('Should return a product', (done) => {
            request
              .get(`/api/product/${id}`)
              .end((err, res) => {
                Object.keys(otherObj).forEach((item) => {
                  expect(res.body[item]).to.be.eql(otherObj[item]);
                });

                resolve(id);
                done(err);
              });
          });
        });
      }))
      .then((id) => new Promise((resolve) => {
        describe('Route DELETE /api/product/{id}', () => {
          it('Should delete a product', (done) => {
            request
              .delete(`/api/product/${id}`)
              .end((err, res) => {
                expect(res.statusCode).to.be.eql(204);
                resolve();
                done(err);
              });
          });
        });
      }));
  });
};
