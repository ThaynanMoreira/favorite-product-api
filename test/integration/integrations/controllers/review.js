/* eslint-disable no-shadow */
const supertest = require('supertest');
const chai = require('chai');
const mocha = require('mocha');
const app = require('../../../../app');

const { it, describe } = mocha;
const request = supertest(app);
const { expect } = chai;

const defaultObj = {
  score: 4,
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere augue et lobortis ullamcorper. Ut lacinia maximus tincidunt.',
  ProductId: 15,
  ClientId: 31,
};
module.exports.test = () => {
  describe('Routes Reviews', () => {
    new Promise((resolve) => {
      describe('Route POST /api/review', () => {
        it('Should create a review', (done) => {
          request
            .post('/api/review')
            .send(defaultObj)
            .end((err, res) => {
              Object.keys(defaultObj).forEach((item) => {
                expect(res.body[item]).to.be.eql(defaultObj[item]);
              });

              done(err);
              resolve(res.body.id);
            });
        });
      });
    })
      .then((id) => new Promise((resolve) => {
        describe('Route DELETE /api/review/{id}', () => {
          it('Should delete a review', (done) => {
            request
              .delete(`/api/review/${id}`)
              .end((err, res) => {
                expect(res.statusCode).to.be.eql(204);
                resolve();
                done(err);
              });
          });
        });
      }));
  });
};
