/* eslint-disable no-undef */
const client = require('./controllers/clients');
const product = require('./controllers/products');
const review = require('./controllers/review');

describe('Routes Index', () => {
  describe('Route GET /health', () => {
    it('Should return a health of system', (done) => {
      request
        .get('/api')
        .end((err, res) => {
          expect(res.text).to.be.eql('Hello to API');
          done(err);
        });
    });
  });
});

client.test();
product.test();
review.test();
