[![pipeline status](https://gitlab.apps.tvglobo.com.br/audiencia/Krawler/badges/master/pipeline.svg)](https://gitlab.apps.tvglobo.com.br/audiencia/Krawler/commits/master)

[![coverage report](https://gitlab.apps.tvglobo.com.br/audiencia/Krawler/badges/master/coverage.svg)](https://gitlab.apps.tvglobo.com.br/audiencia/Krawler/commits/master)

# Favorite Product API

> API designed for clients to favor their products.

## Requirements

- node 10.16.0

## Running in local
``` bash
# Change the file .env.example to .env
mv .env.example .env

# Install dependences
npm install

# Run migrate to database
npm run migrate

# Run API
npm start
```

## Running Docker-Compose
``` bash
# Change the file .env.example to .env
mv .env.example .env

# Build the docker-compose
docker-compose build

# Run API
docker-compose up
```

## Running Tests
``` bash
# Change the file .env.example to .env
mv .env.example .env

# Lint test
npm run test-lint

# Integration tests
npm run test-integration

# With coverage
npm run test-coverage
# or
npm run test-coverage-html
```
