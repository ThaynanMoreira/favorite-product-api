const Sequelize = require('sequelize');
const models = require('../models');

const { Op } = Sequelize;
const limitDefault = 10;

const calculateOffset = (page, limit) => page * limit;

module.exports = {
  getAll: (search, page = 0) => models.Client.findAndCountAll({
    limit: limitDefault,
    offset: calculateOffset(page, limitDefault),
    distinct: true,
    where: {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${(search) || ''}%`,
          },
        },
        {
          email: {
            [Op.like]: `%${(search) || ''}%`,
          },
        },
      ],
    },
    include: [{
      model: models.Product,
      as: 'FavoriteProducts',
    },
    {
      model: models.Review,
      as: 'ReviewsSent',
    }],
  }),

  getById: (id) => models.Client.findOne({
    where: {
      id,
    },
    include: [{
      model: models.Product,
      as: 'FavoriteProducts',
    },
    {
      model: models.Review,
      as: 'ReviewsSent',
    }],
  }),

  create: (body) => models.Client.create(body),

  update: (id, body) => models.Client.update(body, {
    where: {
      id,
    },
  }),

  delete: (id) => models.Client.destroy({
    where: {
      id,
    },
  }),
};
