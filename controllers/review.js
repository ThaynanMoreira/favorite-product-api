const models = require('../models');

module.exports = {

  create: (body) => models.Review.create(body),

  delete: (id) => models.Review.destroy({
    where: {
      id,
    },
  }),
};
