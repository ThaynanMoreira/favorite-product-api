const Sequelize = require('sequelize');
const models = require('../models');

const { Op } = Sequelize;
const limitDefault = 10;

const calculateOffset = (page, limit) => page * limit;

module.exports = {
  getAll: (search, page = 0) => models.Product.findAndCountAll({
    limit: limitDefault,
    offset: calculateOffset(page, limitDefault),
    distinct: true,
    where: {
      [Op.or]: [
        {
          title: {
            [Op.like]: `%${(search) || ''}%`,
          },
        },
        {
          brand: {
            [Op.like]: `%${(search) || ''}%`,
          },
        },
      ],
    },
    include: [{
      model: models.Client,
      as: 'ClientsFavored',
    },
    {
      model: models.Review,
      as: 'ReviewsReceived',
    }],
  }),

  getById: (id) => models.Product.findOne({
    where: {
      id,
    },
    include: [{
      model: models.Client,
      as: 'ClientsFavored',
    },
    {
      model: models.Review,
      as: 'ReviewsReceived',
    }],
  }),

  create: (body) => models.Product.create(body),

  update: (id, body) => models.Product.update(body, {
    where: {
      id,
    },
  }),

  delete: (id) => models.Product.destroy({
    where: {
      id,
    },
  }),

  favoriteProduct: (clientId, productId) => models.Product.findOne({
    where: {
      id: productId,
    },
  })
    .then((product) => product.addClientsFavored(clientId)),
};
